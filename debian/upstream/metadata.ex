# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/leterrier-microtexte/issues
# Bug-Submit: https://github.com/<user>/leterrier-microtexte/issues/new
# Changelog: https://github.com/<user>/leterrier-microtexte/blob/master/CHANGES
# Documentation: https://github.com/<user>/leterrier-microtexte/wiki
# Repository-Browse: https://github.com/<user>/leterrier-microtexte
# Repository: https://github.com/<user>/leterrier-microtexte.git
