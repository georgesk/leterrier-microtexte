#-------------------------------------------------
#
# Project created by QtCreator 2012-10-07T13:27:11
#
#-------------------------------------------------

INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

INCLUDEPATH += /usr/include/terrier
LIBS += -lterrier

QT += core gui script xml network widgets printsupport \
      multimedia multimediawidgets

TARGET   = leterrier-microtexte
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
            version.h

FORMS    += mainwindow.ui

RESOURCES += resources.qrc

win32{
  OTHER_FILES += ../windows/windows.rc
  RC_FILE = ../windows/windows.rc
}

macx {
 ICON = ../macos/icones/abuledu-microtexte.icns
}

