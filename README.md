LETERRIER-MICROTEXTE
====================

LeTerrier-Microtexte is part of the software suite
*"Les logiciels du Terrier"*, whose project was initiated by
the association [AbulÉdu](https://www.abuledu.org). It is also 
known as **AbulEdu-Microtexte**.

This application features a very simple word processor, for K-12 students.

<p style="text-align:center">
<img style="width: 80%" src="https://www.abuledu.org/wp-content/uploads/2015/10/20140804-microtxt_capture02.jpg" alt=""screenshot1">
</p>

In order to make a Debian package, three modifications were made on
the source files, downloaded from 
[https://redmine.abuledu.org/projects/abuledu-microtexte](https://redmine.abuledu.org/projects/abuledu-microtexte)

  1. the subdirectory <code>src/lib</code> has been removed
  2. the file <code>src/src.pro</code> was modified in order to link the
     program against the library <code>libterrier</code>, 
	 whose package is maintained
	 at [https://salsa.debian.org/georgesk/terrier](https://salsa.debian.org/georgesk/terrier)
  3. some parts of the source files were commented out, to prevent
     the package from depending on <code>libpicotts0</code> which is
	 *non-free*
	 
The file <code>debian/control</code> declares a suggestion, to install
a package <code>leterrier-picotts-server</code>, which would enhance 
the word processor by running a local TTS service.
